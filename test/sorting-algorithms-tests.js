// require ../src/sorting-algorithms.js

QUnit.module("Sorting Algorithms");

function runSortTest(functionName, sortFunction) {

    QUnit.test( functionName, function(assert) {
        console.log("=== function: " + functionName + " ===");
        console.log(" ");

        // Arrange sorting test vectors
        var sortTestVectors = [
            {
                title: "Sort an array",
                input: [4,12,7,2,6,10,3,1,9,5],
                expectedResult: [1,2,3,4,5,6,7,9,10,12]
            },
            {
                title: "Sort an empty array",
                input: [],
                expectedResult: []
            },
            {
                title: "Sort an array with the same elements",
                input: [1,1,1,1,1,1,1,1,1,1],
                expectedResult: [1,1,1,1,1,1,1,1,1,1]
            },
        ];

        // Assert sorting tests
        var i, t0, t1, executionTime;

        assert.expect(sortTestVectors.length);

        for (i = 0; i < sortTestVectors.length; i++) {
            console.log(sortTestVectors[i].title);
            console.log("input array: " + sortTestVectors[i].input)

            t0 = performance.now();
            sortFunction(sortTestVectors[i].input);
            t1 = performance.now();
            executionTime = t1 - t0;

            console.log("sorted array: " + sortTestVectors[i].input);
            console.log("");

            assert.deepEqual(sortTestVectors[i].input, sortTestVectors[i].expectedResult,
                sortTestVectors[i].title + " (" + executionTime.toFixed(3) + " ms)");
        }
    });
}

// Execute tests
runSortTest("selectionSort", selectionSort);
runSortTest("insertionSort", insertionSort);
runSortTest("bubbleSort", bubbleSort);
