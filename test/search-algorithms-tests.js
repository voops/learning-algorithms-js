// Require ../test/sample-data.js
// Require ../src/search-algorithms.js


QUnit.module("Search Algorithms");

function runSearchTest(functionName, searchFunction) {

    QUnit.test( functionName, function(assert) {
        console.log("=== function: " + functionName + " ===");
        console.log("");

        // Arrange search test vectors
        var testVectors = [
            {
                title: "Find the middle element",
                input: sampleArray,
                elementToFind: 24,
                expectedResult: 500
            },
            {
                title: "Find the first element",
                input: sampleArray,
                elementToFind: sampleArray[0],
                expectedResult: 0
            },
            {
                title: "Find the last element",
                input: sampleArray,
                elementToFind: 392,
                expectedResult: 999
            },
            {
                title: "If an element doesn't exist, return null",
                input: sampleArray,
                elementToFind: 42,
                expectedResult: null
            },
            {
                title: "If an element isn't unique, return it's first position",
                input: [1, 4, 15, 26, -13, 33, 2, 17, 8, 15],
                elementToFind: 15,
                expectedResult: 2
            },
            {
                title: "If the input array is empty, return null",
                input: [],
                elementToFind: 42,
                expectedResult: null
            }
        ];

        // Assert search tests
        var i, actualResult, t0, t1, executionTime;

        assert.expect(testVectors.length);

        for (i = 0; i < testVectors.length; i++) {
            console.log(testVectors[i].title);
            console.log("expected result: " + testVectors[i].expectedResult);

            t0 = performance.now();
            actualResult = searchFunction(testVectors[i].input, testVectors[i].elementToFind);
            t1 = performance.now();
            executionTime = t1 - t0;

            console.log("actual result: " + actualResult);
            console.log(" ");

            assert.ok( actualResult === testVectors[i].expectedResult,
                testVectors[i].title + " (" + executionTime.toFixed(3) + " ms)");
        }
    });
}

// Execute tests
runSearchTest("primitiveSearch", primitiveSearch);
runSearchTest("sentinelSearch", sentinelSearch);