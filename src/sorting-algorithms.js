function selectionSort(array) {
	var i, j, temp, indexOfSmallest;
	for (i = 0; i < array.length - 1; i++) {
		indexOfSmallest = i;
		for (j = i + 1; j < array.length; j++) {
			if (array[j] < array[indexOfSmallest]) { indexOfSmallest = j }
		}
		temp = array[i];
		array[i] = array[indexOfSmallest];
		array[indexOfSmallest] = temp;
	}
}


function insertionSort(array) {
    var i, j, temp;
    var len = array.length;

    for(i = 1; i < len; i++) {
        temp = array[i];
        for(j = i - 1; j >= 0 && (array[j] > temp); j--) {
            array[j+1] = array[j];
        }
        array[j+1] = temp;
    }
}


function bubbleSort(array) {
    var i, j, temp;
    for (i = 0; i < array.length - 1; i++) {
        for (j = 0; j < array.length - 1 - i; j++) {
            if (array[j] > array[j + 1]) {
                temp = array[j];
                array[j] = array[j + 1];
                array[j + 1] = temp;
            }
        }
    }
}
