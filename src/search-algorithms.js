function primitiveSearch(array, value) {
  var i;
  for (i = 0; i < array.length; i++) {
    if (array[i] === value) { return i; }
  }
  return null;
}

function sentinelSearch(array, value) {  
  if (array.length === 0) { return null }
  var lastValue = array[array.length - 1];
  array[array.length - 1] = value;
  var i = 0;
  while (array[i] != value) {
    i++;
  }
  array[array.length - 1] = lastValue;
  if ((i < array.length - 1) || (array[array.length - 1] == value)) { 
    return i; 
  }
  else return null;
}